/* eslint-env mocha */
import expect from 'expect';
import express from 'express';

import HttpClient from '../src/http-client';

function createMockServer(resData) {
  const app = express();
  app.get('/', (req, res) => res.send(resData));
  return app.listen(0);
}

describe('HttpClient', () => {
  describe('#get', () => {
    it('should work', done => {
      const resData = 'blithe';
      const server = createMockServer(resData);

      const client = new HttpClient({ get: () => 'blammo' }, {
        clientKey: 'foo',
        sharedSecret: 'bar',
        baseUrl: `http://localhost:${server.address().port}`,
      });

      client.get('/').then(res => {
        expect(res.data).toBe(resData);
      }).then(() => {
        server.close();
        done();
      }, err => {
        server.close();
        done(err);
      });
    });
  });
});
