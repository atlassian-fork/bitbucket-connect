/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */
import axios from 'axios';
import expect from 'expect';
import express from 'express';
import md5 from 'md5';

import ExpressAddon from '../src/express-addon';

function createMockServer(addon) {
  const app = express();
  app.use(addon.routerBaseUrl, addon.router);
  return app.listen(0);
}

function createMockRequest(server) {
  return axios.create({
    baseURL: `http://localhost:${server.address().port}`,
    timeout: 2000,
  });
}

function createAddonWithMiddleware() {
  const middleware = (req, res, next) => {
    req.testVar = 'test var'; // eslint-disable-line no-param-reassign
    next();
  };

  return new ExpressAddon(null, { middleware: [middleware] });
}

describe('ExpressAddon', function () {
  function mockAuthenticatedHandler(req, res) {
    res.send(req.testVar);
  }

  it('should serve descriptor through router', function (done) {
    const addon = new ExpressAddon({ name: 'Some test add-on' });
    const server = createMockServer(addon);
    const request = createMockRequest(server);

    request.get('/descriptor.json').then(res => {
      expect(res.data.name).toEqual('Some test add-on');
      done();
    }, done);
  });

  describe('#generateModuleProps', function () {
    it('should generate URL with the routerBaseUrl prepended', function () {
      const addon = new ExpressAddon(null, { routerBaseUrl: '/connect' });
      const props = addon.generateModuleProps('/something', {});
      expect(props.url).toEqual('/connect/something');
    });

    it('should generate key with the keyPrefix prepended', function () {
      const addon = new ExpressAddon();
      const props = addon.generateModuleProps('/something', {}, 'some-prefix-');
      expect(props.key).toEqual(`some-prefix-${md5('/something')}`);
    });

    it('should replace Express URL tokens with Connect URL tokens', function () {
      const addon = new ExpressAddon();
      const props = addon.generateModuleProps('/:repository.uuid/:user.uuid', {});
      expect(props.url).toEqual('/{repository.uuid}/{user.uuid}');
    });

    it('should warn when a URL is passed as a prop', function () {
      const addon = new ExpressAddon();
      const spy = expect.spyOn(addon, 'log');
      addon.generateModuleProps('/something', { url: '/something-else' });
      expect(spy.calls[0].arguments[0]).toInclude('automatically generated');
    });

    it('should warn when a key is passed as a prop', function () {
      const addon = new ExpressAddon();
      const spy = expect.spyOn(addon, 'log');
      addon.generateModuleProps('/something', { key: 'some.module.key' });
      expect(spy.calls[0].arguments[0]).toInclude('automatically generated');
    });
  });

  describe('#registerLifecycleRoute', function () {
    it('should register uninstalled lifecycle hook routes with middleware', function (done) {
      const addon = createAddonWithMiddleware();
      addon.registerLifecycleRoute('/uninstalled', 'uninstalled', mockAuthenticatedHandler);

      const server = createMockServer(addon);
      const request = createMockRequest(server);

      request.post(addon.descriptor.lifecycle.uninstalled).then(res => {
        expect(res.data).toEqual('test var');
        server.close();
        done();
      }, done);
    });

    it('should register installed lifecycle hook routes with no middleware', function (done) {
      const addon = createAddonWithMiddleware();
      addon.registerLifecycleRoute('/installed', 'installed', (req, res) => {
        res.send(req.testVar || 'another test var');
      });

      const server = createMockServer(addon);
      const request = createMockRequest(server);

      request.post(addon.descriptor.lifecycle.installed).then(res => {
        expect(res.data).toEqual('another test var');
        server.close();
        done();
      }, done);
    });
  });

  describe('#registerAdminPageRoute', function () {
    it('should register adminPage routes with middleware', function (done) {
      const addon = createAddonWithMiddleware();
      addon.registerAdminPageRoute('/admin-page', { location: 'org.bitbucket.repository.admin' }, mockAuthenticatedHandler);

      const server = createMockServer(addon);
      const request = createMockRequest(server);

      request.get(addon.descriptor.modules.adminPages[0].url).then(res => {
        expect(res.data).toEqual('test var');
        server.close();
        done();
      }, done);
    });
  });

  describe('#registerConfigurePageRoute', function () {
    it('should register configurePage routes with middleware', function (done) {
      const addon = createAddonWithMiddleware();
      addon.registerConfigurePageRoute('/configure-page', {}, mockAuthenticatedHandler);

      const server = createMockServer(addon);
      const request = createMockRequest(server);

      request.get(addon.descriptor.modules.configurePage[0].url).then(res => {
        expect(res.data).toEqual('test var');
        server.close();
        done();
      }, done);
    });
  });

  describe('#registerFileViewsRoute', function () {
    it('should register fileViews routes with middleware', function (done) {
      const addon = createAddonWithMiddleware();
      addon.registerFileViewsRoute('/file-views', {}, mockAuthenticatedHandler);

      const server = createMockServer(addon);
      const request = createMockRequest(server);

      request.get(addon.descriptor.modules.fileViews[0].url).then(res => {
        expect(res.data).toEqual('test var');
        server.close();
        done();
      }, done);
    });
  });

  describe('#registerProfileTabRoute', function () {
    it('should register profileTab routes with middleware', function (done) {
      const addon = createAddonWithMiddleware();
      addon.registerProfileTabRoute('/profile-tab', {}, mockAuthenticatedHandler);

      const server = createMockServer(addon);
      const request = createMockRequest(server);

      request.get(addon.descriptor.modules.profileTabs[0].url).then(res => {
        expect(res.data).toEqual('test var');
        server.close();
        done();
      }, done);
    });
  });

  describe('#registerRepoPageRoute', function () {
    it('should register repoPage routes with middleware', function (done) {
      const addon = createAddonWithMiddleware();
      addon.registerRepoPageRoute('/repo-page', { location: 'org.bitbucket.repository.navigation' }, mockAuthenticatedHandler);

      const server = createMockServer(addon);
      const request = createMockRequest(server);

      request.get(addon.descriptor.modules.repoPages[0].url).then(res => {
        expect(res.data).toEqual('test var');
        server.close();
        done();
      }, done);
    });
  });

  describe('#registerWebItemRoute', function () {
    it('should register webItem routes with middleware', function (done) {
      const addon = createAddonWithMiddleware();
      addon.registerWebItemRoute('/web-item', { location: 'org.bitbucket.repository.actions' }, mockAuthenticatedHandler);

      const server = createMockServer(addon);
      const request = createMockRequest(server);

      request.get(addon.descriptor.modules.webItems[0].url).then(res => {
        expect(res.data).toEqual('test var');
        server.close();
        done();
      }, done);
    });
  });

  describe('#registerWebPanelRoute', function () {
    it('should register webPanel routes with middleware', function (done) {
      const addon = createAddonWithMiddleware();
      addon.registerWebPanelRoute('/web-panel', { location: 'org.bitbucket.repository.overview.informationPanel' }, mockAuthenticatedHandler);

      const server = createMockServer(addon);
      const request = createMockRequest(server);

      request.get(addon.descriptor.modules.webPanels[0].url).then(res => {
        expect(res.data).toEqual('test var');
        server.close();
        done();
      }, done);
    });
  });

  describe('#registerWebhooksRoute', function () {
    it('should register webhooks routes with middleware', function (done) {
      const addon = createAddonWithMiddleware();
      addon.registerWebhooksRoute('/web-hooks', {}, mockAuthenticatedHandler);

      const server = createMockServer(addon);
      const request = createMockRequest(server);

      request.post(addon.descriptor.modules.webhooks[0].url).then(res => {
        expect(res.data).toEqual('test var');
        server.close();
        done();
      }, done);
    });
  });
});
