/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */
import axios from 'axios';
import expect from 'expect';
import express from 'express';
import jwt from 'atlassian-jwt';

import { getToken, validateJwtToken } from '../src/middleware';

const SECRET = 'super-secret-secret';
const CLIENT_KEY = 'random-client-key';

function createMockRequest(server) {
  return axios.create({
    baseURL: `http://localhost:${server.address().port}`,
    timeout: 2000,
  });
}

describe('getToken', function () {
  function createMockServer(handler) {
    const app = express();
    app.get('/', handler);
    return app.listen(0);
  }

  it('should return query string value first', function (done) {
    const server = createMockServer(req => {
      expect(getToken(req)).toEqual('some-qs-token');
      server.close();
      done();
    });
    const request = createMockRequest(server);
    request.get('/', {
      params: { jwt: 'some-qs-token' },
      headers: { Authorization: 'some-other-token' },
    });
  });

  it('should return Authorization header value if it starts with "JWT"', function (done) {
    const server = createMockServer(req => {
      expect(getToken(req)).toEqual('some-other-token');
      server.close();
      done();
    });
    const request = createMockRequest(server);
    request.get('/', { headers: { Authorization: 'JWT some-other-token' } });
  });

  it('should return `null` if there are no matches', function (done) {
    const server = createMockServer(req => {
      expect(getToken(req)).toEqual(null);
      server.close();
      done();
    });
    const request = createMockRequest(server);
    request.get('/', { headers: { Authorization: 'some-other-token-thats-not-jwt' } });
  });
});

describe('validateJwtToken', function () {
  function createMockServer() {
    const app = express();

    app.use(validateJwtToken(clientKey => {
      if (clientKey === CLIENT_KEY) {
        return Promise.resolve(SECRET);
      }
      return Promise.reject(new Error());
    }));

    app.get('/', (req, res) => {
      res.send();
    });

    return app.listen(0);
  }

  it('should return 200 if JWT token is validated', function (done) {
    const server = createMockServer();
    const request = createMockRequest(server);
    const token = jwt.encode({ things: 'stuff', iss: CLIENT_KEY }, SECRET);

    request.get('/', { params: { jwt: token } }).then(res => {
      expect(res.status).toEqual(200);
      server.close();
      done();
    }, done);
  });

  it('should return 401 if no JWT token is provided', function (done) {
    const server = createMockServer();
    const request = createMockRequest(server);

    request.get('/').catch(res => {
      expect(res.status).toEqual(401);
      expect(res.data).toEqual('No JWT token provided');
      server.close();
      done();
    });
  });

  it('should return 401 if JWT token cannot be decoded', function (done) {
    const server = createMockServer();
    const request = createMockRequest(server);

    request.get('/', { params: { jwt: 'some-jank-token' } }).catch(res => {
      expect(res.status).toEqual(401);
      expect(res.data).toEqual('Error decoding JWT token');
      server.close();
      done();
    });
  });

  it('should return 401 if JWT token signature cannot be verified', function (done) {
    const server = createMockServer();
    const request = createMockRequest(server);
    const token = jwt.encode({ things: 'stuff', iss: CLIENT_KEY }, 'some-other-secret');

    request.get('/', { params: { jwt: token } }).catch(res => {
      expect(res.status).toEqual(401);
      expect(res.data).toEqual('Error validating JWT token signature');
      server.close();
      done();
    });
  });

  it('should return 401 if shared secret cannot be retrieved', function (done) {
    const server = createMockServer();
    const request = createMockRequest(server);
    const token = jwt.encode({ things: 'stuff', iss: 'some-other-client-key' }, 'some-other-secret');

    request.get('/', { params: { jwt: token } }).catch(res => {
      expect(res.status).toEqual(401);
      expect(res.data).toEqual('Error validating JWT token');
      server.close();
      done();
    });
  });
});
