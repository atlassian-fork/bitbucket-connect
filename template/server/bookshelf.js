import bookshelf from 'bookshelf';
import knex from 'knex';

import knexfile from '../knexfile';

export default bookshelf(knex(knexfile[process.env.NODE_ENV || 'development']));
