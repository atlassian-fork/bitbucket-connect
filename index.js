module.exports = {
  Addon: require('./lib/addon').default,
  ExpressAddon: require('./lib/express-addon').default,
  middleware: require('./lib/middleware'),
  HttpClient: require('./lib/http-client').default,
};
