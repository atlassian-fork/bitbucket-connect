import jwt from 'atlassian-jwt';

/**
 * Get JWT token from Express request object
 * @param {Object} req - Express request object
 * @returns {string} JWT token
 */
export function getToken(req) {
  const authorizationHeader = req.get('Authorization');

  if (req.method === 'GET' && req.query.jwt) {
    return req.query.jwt;
  } else if (authorizationHeader && authorizationHeader.toUpperCase().indexOf('JWT ') === 0) {
    return authorizationHeader.slice(4);
  }
  return null;
}

/**
 * Promise-returning function that retrieves
 * shared secret based on client key
 * @name {SharedSecretFn}
 * @function
 * @param {string} clientKey - Connection client key
 * @returns {string} Shared secret between Bitbucket and connection
 */

/**
 * Express middleware generator used to validate JWT token
 * @param {SharedSecretFn} getSharedSecret - Promise-returning function to retrieve shared secret
 * @returns {RouteHandler} Express middleware function
 */
export function validateJwtToken(getSharedSecret) {
  return (req, res, next) => {
    const token = getToken(req);

    if (!token) {
      res.status(401).send('No JWT token provided');
      return;
    }

    let decodedToken;
    try {
      const o = { noVerify: true };
      decodedToken = jwt.decode(token, o.key, o.noVerify);
    } catch (e) {
      res.status(401).send('Error decoding JWT token');
      return;
    }

    const { iss: clientKey } = decodedToken;

    getSharedSecret(clientKey).then(sharedSecret => {
      try {
        jwt.decode(token, sharedSecret);
        req.clientKey = clientKey; // eslint-disable-line no-param-reassign
        next();
      } catch (err) {
        res.status(401).send('Error validating JWT token signature');
      }
    }).catch(() => {
      res.status(401).send('Error validating JWT token');
    });
  };
}
